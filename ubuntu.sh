#! /bin/bash

#ubuntu update

ADD_SWAP=0
ADD_DOCKER=0
ADD_NVM=0

for arg in "$@"; do
  shift
  case "$arg" in
    "--swap") ADD_SWAP=1 ;;
	"--docker") ADD_DOCKER=1 ;;
	"--nvm") ADD_NVM=1
  esac
done

if [ $# = 0 ]; then
	echo "No options specified, enabling all options."
	ADD_SWAP=1
	ADD_DOCKER=1
	ADD_NVM=1
fi

if [ $ADD_SWAP = 1 ]; then
	echo "Enabling swap"

	output=$(sudo swapon --show)
	if [ -z "$output" ] ; then 
		sudo fallocate -l 2G /swapfile
	  	ls -lh /swapfile
		sudo chmod 600 /swapfile
	  	ls -lh /swapfile
	  	sudo mkswap /swapfile
	    sudo swapon /swapfile
		sudo swapon --show
		free -h
	  	sudo cp /etc/fstab /etc/fstab.bak
	  	echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
		sudo sysctl vm.swappiness=10
	  	cat /proc/sys/vm/swappiness
		sudo sysctl vm.vfs_cache_pressure=50
		cat /proc/sys/vm/vfs_cache_pressure
		sudo cp /etc/sysctl.conf /etc/sysctl.conf.bak
		echo 'vm.vfs_cache_pressure=50' | sudo tee -a /etc/sysctl.conf
	else
		echo "Swap already enabled."
	fi
fi

echo "# Update repository sources"
sudo apt-get update \
  && sudo apt-get dist-upgrade -y

echo "# Install dependencies"
sudo apt-get install -y \
  git \
  jq \
  nfs-common \
  build-essential \
  cifs-utils \
  apt-transport-https \
  ca-certificates \
  curl \
  autofs \
  smbclient \
  parted \
  gnupg-agent \
  software-properties-common \
  sqlite3


if [ $ADD_DOCKER = 1 ]; then
	echo "docker install"

	# Add Docker's official GPG key
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

	# Set up the stable repo
	sudo add-apt-repository \
	   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
	   $(lsb_release -cs) \
	   stable"

	# Update the packages
	sudo apt-get update

	# Install docker-ce
	sudo apt-get install -y \
	  docker-ce \
	  docker-ce-cli \
	  containerd.io docker-compose

	# Access docker w/o sudo
	sudo usermod -aG docker q
fi

#disable cloud init
sudo touch /etc/cloud/cloud-init.disabled

if [ $ADD_NVM = 1 ]; then
	echo "nvm install"
	echo "# Get current lts version of node"
	latest_nvm_version=$(curl -L --silent "https://api.github.com/repos/nvm-sh/nvm/releases/latest" | jq -r .tag_name)

	echo "node install, pulling version: $latest_nvm_version, url: https://raw.githubusercontent.com/nvm-sh/nvm/$latest_nvm_version/install.sh"
	curl -L "https://raw.githubusercontent.com/nvm-sh/nvm/$latest_nvm_version/install.sh" | bash
	source ~/.nvm/nvm.sh
	nvm install --lts
	nvm use --lts
fi

#shell-settings install
echo "# install bash settings"
git clone https://bitbucket.org/calisto/shell-settings ~/.qsh
cd ~/.qsh && ./install.sh