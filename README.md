# Install Scripts

## Ubuntu

#### Command  

```bash
curl -o- https://bitbucket.org/calisto/install/raw/master/ubuntu.sh | bash
```

Possible Options

- `--swap` sets up a swapfile on the system, this is useful for digitalocean instances
- `--docker` installs current version of docker
- `--nvm` installs nvm for easy node installs
- specifying no options installs everything